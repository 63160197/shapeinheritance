/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.shapeinheritance;

/**
 *
 * @author Nippon
 */
public class Square extends Rectangle {
    private double s;

    public Square(double s) {
        super(s, s);
        System.out.println("Square Created");
        this.s = s*s;
    }

    @Override
    public double CalArea() {
        return s * s;
    }

}
